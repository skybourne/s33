// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
// Using the data retrieved, create an array using the map() method to return just the title of every item and print the result in the console.
fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response => response.json())
	.then(json => {
		const toDoTitles = json.map(item => item.title);
		console.log(toDoTitles);
	})


// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(response => response.json())
	.then(todo => {
		console.log(todo);
		console.log(`The item "${todo.title}" on the list has a status of ${todo.completed}`)})

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos' , {
	method: "POST",
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// Update a do to list item by changing the data stucture to contain the following properties:
// a. Title
// b. Description
// c. Status
// d. Date Completed
// e. User ID

fetch('https://jsonplaceholder.typicode.com/todos/1' , {
	method: "PUT",
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data stucture",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1' , {
	method: "PATCH",
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1' , {
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json))





	